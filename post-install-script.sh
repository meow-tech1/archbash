mount /dev/sda3 /boot/EFI
timedatectl set-timezone Asia/Hong_Kong
locale-gen
echo LANG=en_GB.UTF-8 > /etc/locale.conf
export LANG=en_GB.UTF-8
echo ArchLinux > /etc/hostname
touch /etc/hosts
printf "127.0.0.1	localhost\n::1		localhost\n127.0.1.1	ArchLinux" > tmp.txt
echo Set Your Password.
passwd
pacman -S grub efibootmgr
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg
pacman -S xorg
exit