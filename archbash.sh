echo OFF
echo /dev/sda1 ROOT
echo /dev/sda2 SWAP
echo /dev/sda3 BOOT/EFI 500MB
mkswap /dev/sda2
swapon /dev/sda2
mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt
mkdir /mnt/boot/EFI
mkfs.fat -F32 /dev/sda3
mount /dev/sda3 /mnt/boot/EFI
pacstrap /mnt base linux linux-firmware nano sudo leafpad
genfstab -U /mnt >> /mnt/etc/fstab
chmod +x post-install-script.sh
cp post-install-script.sh /mnt/post-install-script.sh
arch-chroot /mnt ./post-install-script.sh
reboot